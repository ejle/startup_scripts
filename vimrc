" let g:ale_emit_conflict_warnings = 0
"let g:solarized_termcolors=256
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:jsx_ext_required = 0
set guifont=PragmataPro:h16
set tw=100
set mouse=a
hi Normal guibg=NONE ctermbg=NONE
let g:ale_python_pylint_options = "--init-hook='import sys; sys.path.append(\"/home/edward/projects\")'"

set cursorline " set cursor line
set hlsearch " set highlight search 
hi Search guibg=LightBlue
                            
set hidden " allows switching of buffers without saving
" set persistent undo
set undofile                " Save undos after file closes
set undodir=$HOME/.vim/undo " where to save undo histories NOTE: folder must exist
set undolevels=5000         " How many undos
set undoreload=50000        " number of lines to save for undo

" draw line at 80 characters
set colorcolumn=101

" vundle install:
"https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/
set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

"Add plugins here (note older versions of Vundle used Bundle instead of Plugin)

" Vundle plugin for smarter folding python
Plugin 'tmhedberg/SimpylFold'

" Plugin for auto-indenting python
Plugin 'vim-scripts/indentpython.vim'

" Plugin for auto-completing python
Plugin 'Valloric/YouCompleteMe'

" Plugin for commenting
Plugin 'tpope/vim-commentary'

" Plugin for syntax checking/highlighting
" Plugin 'scrooloose/syntastic'

" Add PEP8checking
Plugin 'nvie/vim-flake8'

" Color schemes
Plugin 'altercation/vim-colors-solarized'

" File browsing
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'

" Monokai theme
Plugin 'crusoexia/vim-monokai'

" Super searching
Plugin 'kien/ctrlp.vim'

" Git integration
Plugin 'tpope/vim-fugitive'

" powerline
" Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

" airline - pure vimscript no python
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" promptline for shell in vim
Plugin 'edkolev/promptline.vim'

" lots of colorthemes
Plugin 'flazz/vim-colorschemes'

" for javascript development in vi - 
" https://drivy.engineering/setting-up-vim-for-react/"
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'

" generating async commands for vim
" Plugin 'idbrii/AsyncCommand'
" Plugin 'pydave/AsyncCommand'

" running syntastic async
" Plugin 'stgpetrovic/syntastic-async'

" Plugin for auto dealing with swap files
Plugin 'gioele/vim-autoswap'

" Plugin for async linter engine (requires linters separately)
" >> conflicts with syntastic
Plugin 'w0rp/ale'

" Plugin for vimtex
Plugin 'lervag/vimtex'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required


let g:SimpylFold_docstring_preview=1

let g:ycm_autoclose_preview_window_after_completion=1		
" closes when done with autocomplete

" custom keys
" let mapleader=","
"map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
" nmap <Leader>b :CtrlPBuffer<CR>

" turn on numbering
set nu

" split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" no arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" smart buffer navigation
nnoremap <Tab> :bnext<CR>
nnoremap <S-Tab> :bprevious<CR>
nnoremap <C-X> :bdelete<CR>

" move to beginnign and end of line in insert mode
inoremap <C-e> <Esc>A
inoremap <C-a> <Esc>I

" for indenting
set expandtab
set shiftwidth=4
set autoindent

" let vim reload the config every time it changes
augroup myvimrc
    au!
    au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $myGVIMRC | endif
augroup END


"------------Vimtex on Ubuntu stuff -----------
let mapleader=","
let maplocalleader=","
let g:tex_flabor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_latexlog={'font':0}
let g:vimtex_quickfix_autoclose_after_keystrokes=5


"------------Start Python PEP 8 stuff----------------
" Number of spaces that a pre-existing tab is equal to.
au BufRead,BufNewFile *py,*pyw,*.c,*.h set tabstop=4 
"spaces for indents au BufRead,BufNewFile *.py,*pyw set 
"shiftwidth=4 au BufRead,BufNewFile *.py,*.pyw set expandtab

au BufRead,BufNewFile *.py set softtabstop=4

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" Wrap text after a certain number of characters
au BufRead,BufNewFile *.py,*.pyw, set textwidth=100

" Use UNIX (\n) line endings.
au BufNewFile *.py,*.pyw,*.c,*.h set fileformat=unix

" Set the default file encoding to UTF-8:
set encoding=utf-8

" For full syntax highlighting:
let python_highlight_all=1
syntax on

" Keep indentation level from previous line:
autocmd FileType python set autoindent

" make backspaces more powerfull
set backspace=indent,eol,start


" Enable folding
set foldmethod=indent
set foldlevel=99

"Folding based on indentation:
autocmd FileType python set foldmethod=indent


"----------Stop python PEP 8 stuff--------------

"python with virtualenv support
"py << EOF
"import os
"import sys
"if 'VIRTUAL_ENV' in os.environ:
"  project_base_dir = os.environ['VIRTUAL_ENV']
"  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"  execfile(activate_this, dict(__file__=activate_this))
"EOF



"logic for colorings
if has ('gui_running')
    set background=dark
    " highlight Normal ctermfg=grey ctermbg=black
    colorscheme solarized	
else 
    syntax enable
    set background=dark
    colorscheme solarized 
endif

" make vim background transparent
hi Normal guibg=NONE ctermbg=NONE

" Toggle dark and light theme with F5?
call togglebg#map("<F5>")

" for hiding .pyc files in the file browsing
let NERDTreeIgnore=['\.pyc$', '\~$']

" for powerline stuff
set rtp+=$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/

" Always show statusline
set laststatus=2

" Use 256 colours (Use this setting only if your terminal supports 256 colours)
"set t_Co=256

" change cursor in inser and normal mode
if has("autocmd")
    au VimEnter,InsertLeave * silent execute '!echo -ne "\e[1 q"' | redraw!
    au InsertEnter,InsertChange *
        \ if v:insertmode == 'i' | 
        \   silent execute '!echo -ne "\e[5 q"' | redraw! |
        \ elseif v:insertmode == 'r' |
        \   silent execute '!echo -ne "\e[3 q"' | redraw! |
        \ endif
    au VimLeave * silent execute '!echo -ne "\e[ q"' | redraw!
endif
"make esc jk or kj
imap jk <ESC>
imap kj <ESC>

" make colon space and nop the normal colon
nnoremap <Space> :
nnoremap : <NOP>
let g:ackprg = 'ag --nogroup --nocolor --column'

" remove trailing whitespace automatically
autocmd BufWritePre *.py :%s/\s\+$//e 

" title option is enabled 
set title titlestring=
