# colored terminals
export CLICOLOR=Yes
# export LSCOLORS=GxFxCxDxBxegedabagaced
export LSCOLORS="Gxfxcxdxbxegedabagacad"

export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH=$HOME:$PATH
export PATH=$HOME/projects/ampl_submitter:$PATH
export PATH=$HOME/projects/amplide.linux64:$PATH
# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="bullet-train"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=1

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  autojump
  autopep8
  common-aliases
  compleat
  git
  gitfast
  git-extras
  github
  history
  pep8
  pyenv
  pylint
  python
  tmux
  vi-mode
  zsh-vim-mode
  zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh

## binding for vi-mode
bindkey jk vi-cmd-mode
bindkey kj vi-cmd-mode

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim'
fi


## for vi-mode in oh-my-zsh and custom display of modes ============================================
# Updates editor information when the keymap changes.
# function zle-keymap-select() {
#   # update keymap variable for the prompt
#   VI_KEYMAP=$KEYMAP

#   zle reset-prompt
#   zle -R
# }

# zle -N zle-keymap-select

# function vi-accept-line() {
#   VI_KEYMAP=main
#   zle accept-line
# }

# zle -N vi-accept-line

# bindkey -v

# # use custom accept-line widget to update $VI_KEYMAP
# bindkey -M vicmd '^J' vi-accept-line
# bindkey -M vicmd '^M' vi-accept-line

# # allow v to edit the command line (standard behaviour)
# autoload -Uz edit-command-line
# zle -N edit-command-line
# bindkey -M vicmd 'v' edit-command-line

# # allow ctrl-p, ctrl-n for navigate history (standard behaviour)
# bindkey '^P' up-history
# bindkey '^N' down-history

# # allow ctrl-h, ctrl-w, ctrl-? for char and word deletion (standard behaviour)
# bindkey '^?' backward-delete-char
# bindkey '^h' backward-delete-char
# bindkey '^w' backward-kill-word

# # allow ctrl-r and ctrl-s to search the history
# bindkey '^r' history-incremental-search-backward
# bindkey '^s' history-incremental-search-forward

# # allow ctrl-a and ctrl-e to move to beginning/end of line
# bindkey '^a' beginning-of-line
# bindkey '^e' end-of-line

# # if mode indicator wasn't setup by theme, define default
# if [[ "$MODE_INDICATOR" == "" ]]; then
#   MODE_INDICATOR="%{$fg_bold[red]%}<%{$fg[red]%}<<%{$reset_color%}"
# fi

# function vi_mode_prompt_info() {
#   echo "${${VI_KEYMAP/vicmd/$MODE_INDICATOR}/(main|viins)/}"
# }

# # define right prompt, if it wasn't defined by a theme
# if [[ "$RPS1" == "" && "$RPROMPT" == "" ]]; then
#   RPS1='$(vi_mode_prompt_info)'
# fi




# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
alias zshconfig="mate ~/.zshrc"
alias ohmyzsh="mate ~/.oh-my-zsh"

# short aliases for things:
alias py='python'
alias v='vi'
alias vimrc='vi ~/.vimrc'
alias zshrc='vi ~/.zshrc'
alias i3config='vi ~/.config/i3/config'
alias nt='nautilus --no-desktop&'
# alias setupenv="virtualenv -p /usr/bin/python3 env;echo 'source env/bin/activate' > .envrc;direnv allow ."
alias agg="ag -g"
# for dircolors in solarized
# eval `dircolors $HOME/.dir_colors/dircolors`

# hook together direnv to work with zsh
# eval "$(direnv hook zsh)"

source /etc/zsh_command_not_found

# eval `feh --bg-scale /home/edwardlee/Pictures/polygon.jpg`
#
# eval `xcompmgr -c -f -n`

## reset key bindings (these will depend on the system)
# make CapsLock behave like Ctrl:
setxkbmap -option 'caps:ctrl_modifier'
# make short-pressed Ctrl be an escape
xcape -e 'Caps_Lock=Escape;Control_L=Escape;Control_R=Escape'
# (for OSX style keyboards): swap left-super and left-alt
# xmodmap -e "keycode 64 = Super_L NoSymbol Super_L"
# xmodmap -e "keycode 133 = Alt_L Meta_L Alt_L Meta_L"

source $HOME/env/bin/activate

# command to mount all storage (automate this?)
# alias mnt='sudo mount -t nfs pql-syd-nas01.preludequant.net:/storage /storage_office/;sudo mount -t nfs pql-alc-nas01.preludequant.net:/storage /storage/'

# for prelude pythonpath:
# export PYTHONPATH='/home/edwardlee/projects:/home/edward/projects/pqr/tools:/storage/common/research/python_alphas'
# export GM='/storage/common/research/opt'
# export USE_NEW_DATA='True'
# export LOCAL_GRID='False'
# export DATA_PATH='/storage/common/marketdata'

# alias for tools (mostly just the path)
# alias bt="python /home/edward/projects/pqr/analysis/backtest.py"
# alias backtest="python /home/edward/projects/pqr/analysis/backtest.py -v"
# alias st="python /home/edward/projects/pqr/analysis/sensitivity.py"
# alias cc="python /home/edward/projects/pqr/analysis/config_compare.py -v"
# alias br="python /home/edward/projects/pqr/analysis/bulk_runner.py"
# alias fa="python /home/edward/projects/pqr/analysis/fine_analysis.py"
# alias fine="python /home/edward/projects/pqr/analysis/fine_analysis.py"
# alias rn="python /home/edward/projects/pqr/optimizer/run.py"
#alias run="python /home/edward/projects/pqr/optimizer/run.py"
#alias ft="python /home/edward/projects/pqr/optimizer/grid/fetch_specific_from_opt.py"
#alias fetch="python /home/edward/projects/pqr/optimizer/grid/fetch_specific_from_opt.py"
#alias nc="python /home/edward/projects/pqr/analysis/normalise_config.py"
#alias sa="python /home/edward/projects/pqr/analysis/signal_analysis.py --html"
#alias sa2="python /home/edward/projects/pqr/analysis/signal_analysis2.py"
#alias mdstat="python /home/edward/projects/pqr/analysis/md_stats.py"
#alias static="python /home/edward/projects/pqr/analysis/static_data.py"
#alias price="python /home/edward/projects/pqr/analysis/price_range.py"
#alias refdata="vi -M /storage/common/refdata/universe.json"
#alias gui="python /home/edward/projects/pqr/analysis/gui_analysis.py"
#alias run_prior="/storage/common/software/phoenix/prod/run_prior"
#alias check_grid="ssh pqgrid@gm 'cd /opt/jppf/GridInterface/log && tail -10000 log.log | grep UserQu'"
#alias r2="python /home/edward/projects/pqr/optimizer/r2signal/r2evaluator.py"
#alias instrument="python /home/edward/projects/pqr/tools/instrument_config.py"
#alias run_prior="run_prior --date"
#alias results="libreoffice /storage/common/research/opt/edward/config_results.xls &"
#alias range="python /home/edward/projects/pqr/analysis/price_range.py"
#alias sl="python /home/edward/projects/pqr/optimizer/setup_launch.py"
# alias e="cd /storage/common/research/opt/edward/"
#alias e="cd /storage/common/research/opt/edward/`ls /storage/common/research/opt/edward/ | grep ^201 | tail -1`"
alias b="bash"
alias z="zsh"
#alias todo="vi /storage/common/research/opt/edward/notes/todo.txt"
#alias visual="python /home/edward/projects/pqr/analysis/visualize_result.py"
#alias flow_price="python3 /home/edward/projects/pqr/analysis/flow_price.py"
#alias stop="python3 /home/edward/projects/pqr/analysis/stop_analysis.py"
#alias xtree="python /home/edward/projects/pqr/tools/xtree.py"
#alias export_matches="/home/edward/projects/pqr/tools/git_export_matches.sh"
#alias opp="/home/edward/projects/pqr/analysis/opportunity.py"
#alias dir_today="mkdir /storage/common/research/opt/edward/`date +'%Y%m%d'`"
alias c="clear"
#alias pa="$HOME/projects/pqr/python_alphas/pa_common.py"

# set sean's engine to current path
# export PATH=/storage/common/software/phoenix/prod:$PATH
# we are really using simulation and prod is kind of different (msg_xlate, etc)
# export PATH=/storage/common/software/phoenix/simulation:$PATH
#export PATH=/storage/common/software/phoenix/prod:$PATH
#export GM=/storage/common/research/opt

# import migration functions:
#source $HOME/projects/pqr/tools/migration.sh

alias gd='git diff'
alias ga='git add'
alias gs='git status'
alias gc='git commit'
alias gl='git log'
alias chrome='google-chrome'
alias mdr='mkdir'
alias dropbox='/home/edward-phd/.dropbox/dropbox.py'
alias copy='xclip -sel clip'
alias jn="jupyter notebook"

# quantlib specific commands
export PYTHONPATH=$HOME/projects
alias run=$HOME/projects/quantlib/optimizer/run.py
alias xtree=$HOME/projects/quantlib/tools/xtree.py

