Startup guide for ubuntu systems:

- Install apple notes client: https://snapcraft.io/icloud-notes-linux-client

- Sudo apt install: 
        - python-dev
        - virtualenv
        - libreoffice
        - autojump
        - silversearcher-ag
        - command not found??

- Fonts (included):
     Pragmata Pro.

- Zsh + oh-my-zsh
        zsh-autosuggestions: https://github.com/zsh-users/zsh-autosuggestions
        zsh-vim-mode: https://github.com/ejle/zsh-vim-mode

 -keyboard: (built in zshrc)
        caps to control and escape
        http://www.economyofeffort.com/2014/08/11/beyond-ctrl-remap-make-that-caps-lock-key-useful/

- vim:
    (Search vim 8.0 python support Pi-rho/dev ppa)
    https://askubuntu.com/questions/775059/vim-python-support-on-ubuntu-16-04
    - vundle, PluginInstall
    - YCM manual installation

    Vim cursor change in mode
    http://vim.wikia.com/wiki/Change_cursor_shape_in_different_modes

- i3WM: (after all the other stuff)
    - https://www.maketecheasier.com/install-use-i3-window-manager-ubuntu/
    - i3 gaps: add PPA:
	https://launchpad.net/~kgilmer/+archive/ubuntu/speed-ricer
    - sudo apt-get install i3-gaps-wm

# NOTE: old, ignore
    - https://hackmd.io/s/By6mF5Rqb
        (NEW) 
    - https://benjames.io/2017/09/03/installing-i3-gaps-on-ubuntu-16-04/
    - Need terminal transparency 
    https://faq.i3wm.org/question/3162/terminal-transparency-in-i3.1.html

Change dpi for i3 bar: xrandr - - dpi 150
Also can use xrandr for external monitor dpi scaling.

Brightness controls
Volume controls: install volume icon-alsa 
Play pause

Utility setup:
chrome/Dashlane
Ssh + aws (only work on work comp)
Email (“”) with mailspring


